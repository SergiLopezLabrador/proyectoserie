package dto;

public class proyectoSerieDto {
	
	//Aqu� creo las variables tipo private para configurar los constructores
	private String titulo;
	private int numTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	
	//Aqu� creo un constructor con todos los valores default
	public proyectoSerieDto() {
        this.titulo=" ";
        this.numTemporadas=3;
        this.entregado=false;
        this.genero=" ";
        this.creador=" ";
    }


	//Aqu� creo un constructor donde genero, temporadas y entregado s�n defaults
    public proyectoSerieDto(String titulo, String creador) {
        this.titulo = titulo;
        this.creador = creador;
        this.numTemporadas=3;
        this.entregado=false;
        this.genero=" ";
    }


    //Aqu� hago un constructor con todos los atributos
    public proyectoSerieDto(String titulo, int numeroTemporadas, String genero, String creador) {
        this.titulo = titulo;
        this.numTemporadas = numeroTemporadas;
        this.genero = genero;
        this.creador = creador;
    }

  //Aqu� convierto los constructores a string para que al Main salga bien visualmente
	@Override
	public String toString() {
		return "proyectoSerieDto [titulo=" + titulo + ", numTemporadas=" + numTemporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	
	
}
